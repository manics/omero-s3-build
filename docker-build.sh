#!/bin/sh

set -e

REPO="${CI_REGISTRY_IMAGE:-omero-server-s3}"
TAG=`cat VERSION.txt`

cp openmicroscopy/target/OMERO.server-$TAG.zip docker/OMERO.server.zip

docker build -t "$REPO:$TAG" docker

if [ -n "$CI_JOB_TOKEN" ]; then
  docker push "$REPO:$TAG"
fi
